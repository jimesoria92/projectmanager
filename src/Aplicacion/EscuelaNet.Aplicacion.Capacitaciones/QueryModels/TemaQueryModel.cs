﻿using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryModels
{
    public class TemaQueryModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public NivelTema Nivel { get; set; }
        public int? Total { get; set; }
    }
}