﻿using Autofac;
using Autofac.Integration.Mvc;
using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura.AutofacModules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.App_Start
{
    //es para usar unity y/o autofac
    public class IoConfiguration //tiene un metodo estatico, que obliga a instanciar en el minuto cero "como una constante"
    {
        public static void ConfigurarIoC() //se agrega un objeto constructor
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly()); //aqui solo se fija en los namespace de este proyectp

            var container = builder.Build(); //aq
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));// el que resuelve todas las dependencia es mi hilo de autofac
            builder.RegisterModule(new ApplicationModule);
        } 
    }
}