﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Proyectos;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class TecnologiasIndexModel
    {
        public string Nombre { get; set; }
        public List<Tecnologias>Tecnologias { get; set; }
    }
}