﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class NuevaEtapaModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int duracion { get; set; }
        public int IDLinea { get; set; }
        public int IDProy { get; set; }
    }
}