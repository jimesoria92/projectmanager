﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();

        private Contexto()
        {

            this.LineasDeProduccion = new List<LineaDeProduccion>();
            LineasDeProduccion.Add(new LineaDeProduccion("Mobile"));
            LineasDeProduccion[0].pushProyecto("Proyecto Chenoville", "Planta nuclear a punto de explotar", "Mario de la Barra", 3815056723, "mario_del_VaLlE@outlook.com");
            LineasDeProduccion[0].Proyectos[0].PushEtapa("Desarrollo", 9);

            LineasDeProduccion.Add(new LineaDeProduccion("API's"));
            LineasDeProduccion[1].pushProyecto("Proyecto C#", "Sistema WEB", "Estebanquito", 381510161, "elasdjqko@outlook.com");
            LineasDeProduccion[1].Proyectos[0].PushEtapa("BugFix", 10);
            LineasDeProduccion.Add(new LineaDeProduccion("Gestión Empresarial"));
            LineasDeProduccion[2].pushProyecto("CSS", "Responsive", "Alejo", 3815056423, "asldñasdñE@outlook.com");
            LineasDeProduccion[2].Proyectos[0].PushEtapa("Alejo", 10);

            this.Tecnologias = new List<Tecnologias>();
            Tecnologias.Add(new Tecnologias("Progressive web aplication"));

        }
        public List<LineaDeProduccion> LineasDeProduccion { get; set; }
        public List<Tecnologias> Tecnologias { get; set; }
        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }

    }
}