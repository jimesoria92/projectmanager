﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.Repositorios;

namespace EscuelaNet.Presentacion.Proyectos.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule: Autofac.Module
    {
        protected override void Load(ContainerBuilder builder) //una agrupacion de dependencias que se registran juntas
        {
            builder.RegisterType<LineaContexto>()
                .InstancePerRequest;
            builder.RegisterType<LineaRepository>().As<ILineaRepository>()//registra esta linea de repositorio y entregalo cada vez que alguien pida una linea de repositorio
                .InstancePerMatchingLifetimeScope();
            base.Load(builder);
        }


    }
}