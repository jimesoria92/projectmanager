﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.Repositorios;
//using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class LineasController : Controller
    {
       
        // GET: Default
        private ILineaRepository _lineaRepository;
        public LineasController(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }
        public ActionResult Index()
        {
            var lineasDeProduccion = _lineaRepository.ListLinea();
            var model = new LineasIndexModel()
            {
                Titulo = "Primera pruba",
                LineasDeProduccion = lineasDeProduccion
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaLineaModel();
            return View(model);

        }
        [HttpPost]
        public ActionResult New(NuevaLineaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    _lineaRepository.Add(new LineaDeProduccion(model.Nombre));
                    _lineaRepository.UnitOfWork.SaveChanges();                    
                    TempData["success"] = "Linea de producción creada";
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
               
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Edit(int id)
        {
            var linea = _lineaRepository.GetLinea(id);
            var model = new NuevaLineaModel() {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(NuevaLineaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var linea = _lineaRepository.GetLinea(model.Id);
                    linea.Editar(model.Nombre);
                    _lineaRepository.Update(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Linea de producción editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        public ActionResult Delete(int id)
        {
            var linea = _lineaRepository.GetLinea(id);
            var model = new NuevaLineaModel()
            {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevaLineaModel model)
        {
           
                try
                {
                    var linea = _lineaRepository.GetLinea(model.Id);
                    _lineaRepository.Delete(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Linea de producción borrada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

           

        }


    }
}