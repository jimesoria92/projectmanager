﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class ProyectosController : Controller
    {
        // GET: Proyectos
        private ILineaRepository _lineaRepository;
        public ProyectosController(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }
        public ActionResult Index(int id)
        {
            var linea = Contexto.Instancia.LineasDeProduccion[id];
            var model = new ProyectoIndexModel();
          
            model.Titulo = string.Format("Proyecto de {0}", linea.Nombre);
            model.Proyectos = (List<Proyecto>)linea.Proyectos;
            model.Linea = id;
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevoProyectoModel();
            return View(model);
        }

        [HttpPost]

        public ActionResult New(NuevoProyectoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].pushProyecto(
                        model.Nombre, model.Descripcion, model.NombreResponsable, model.TelefonoResponsable, model.EmailResponsable
                        );
                    TempData["success"] = "Proyecto creado";
                    return RedirectToAction("Index/"+model.IDLinea);
                }catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int idProy, int idLinea)
        {

            var proyecto = Contexto.Instancia.LineasDeProduccion[idLinea].Proyectos[idProy];
            var model = new NuevoProyectoModel()
            {
                Nombre = proyecto.Nombre,
                Id = idProy,
                Descripcion = proyecto.Descripcion,
                NombreResponsable = proyecto.NombreResponsable,
                TelefonoResponsable = proyecto.TelefonoResponsable,
                EmailResponsable = proyecto.EmailResponsable,
                IDLinea = idLinea
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult Edit(NuevoProyectoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre) || !string.IsNullOrEmpty(model.Descripcion))
            {
                try
                {
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.Id].Nombre = model.Nombre;
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.Id].Descripcion = model.Descripcion;
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.Id].NombreResponsable = model.NombreResponsable;
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.Id].TelefonoResponsable = model.TelefonoResponsable;
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.Id].EmailResponsable = model.EmailResponsable;

                    TempData["success"] = "Proyecto modificado";
                    return RedirectToAction("Index", new { id= model.IDLinea });
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Se pasó un nombre o descripcion vacio/erroneo.";
                return View(model);
            }
        }

        public ActionResult Delete(int idLinea, int idProy)
        {
            var proyecto = Contexto.Instancia.LineasDeProduccion[idLinea].Proyectos[idProy];
            var model = new NuevoProyectoModel()
            {
                Nombre = proyecto.Nombre,
                Id = proyecto.ID
            };
            return View(model);

        }


        [HttpPost]

        public ActionResult Delete(NuevoProyectoModel model)
        {
            try
            {
                Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos.RemoveAt(model.Id);
                TempData["success"] = "Proyecto eliminado";
                return RedirectToAction("Index", new { id = model.IDLinea});
            }
            catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

    }
}