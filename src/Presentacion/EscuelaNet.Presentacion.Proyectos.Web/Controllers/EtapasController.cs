﻿using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class EtapasController : Controller
    {
        // GET: Etapa
        public ActionResult Index(int idLinea, int idProy)
        {

            var etapas = Contexto.Instancia.LineasDeProduccion[0].Proyectos[0];
            var model = new EtapaIndexModel();
            model.Nombre = string.Format("Etapa {0}", etapas.Nombre);
            model.Etapas = (List<Etapa>)etapas.Etapas;
            model.IDLinea = idLinea;
            model.IDProy = idProy;

            return View(model);

        }

        public ActionResult New()
        {
            var model = new NuevaEtapaModel();
            return View(model);
        }

        [HttpPost]

        public ActionResult New(NuevaEtapaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.IDProy].PushEtapa(model.Nombre, model.duracion);
                    TempData["success"] = "La etapa se ha creado";
                    return RedirectToAction("Index", new { idLinea = model.IDLinea, idProy = model.IDProy });
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            } else
            {
                TempData["error"] = "Debe completar ambos campos ";
                return View(model);
            }
        }

        public ActionResult Edit(int idProy, int idLinea, int idEtapa)
        {
            var etapas = Contexto.Instancia.LineasDeProduccion[idLinea].Proyectos[idProy].Etapas[idEtapa];
            var model = new NuevaEtapaModel()
            {
                Nombre = etapas.Nombre,
                ID = idEtapa,
                duracion = etapas.Duracion,
                IDLinea = idLinea,
                IDProy = idProy
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult Edit(NuevaEtapaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.IDProy].Etapas[model.ID].Nombre = model.Nombre;
                    Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.IDProy].Etapas[model.ID].Duracion = model.duracion;

                    TempData["success"] = "Etapa modificada";
                    return RedirectToAction("index", new { idLinea=model.IDLinea,idProy=model.IDProy }); 
                }
                catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Debe completar ambos campos";
                return View(model);
            }
        }

        public ActionResult Delete(int idLinea, int idProy, int idEtapa)
        {
            var etapas = Contexto.Instancia.LineasDeProduccion[idLinea].Proyectos[idProy].Etapas[idEtapa];
            var model = new NuevaEtapaModel()
            {
                Nombre = etapas.Nombre,
                ID = idEtapa
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(NuevaEtapaModel model)
        {
            try
            {
                Contexto.Instancia.LineasDeProduccion[model.IDLinea].Proyectos[model.IDProy].Etapas.RemoveAt(model.ID);
                TempData["success"] = "Etapa eliminada";
                return RedirectToAction("Index", new { idLinea=model.IDLinea, idProy = model.IDProy });
            }
            catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }



    }
}