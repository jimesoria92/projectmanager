﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Archivos.Web.Models
{
    public class NuevoArchivoModel
    {
        public String Nombre { get; set; }
        public String Tipo { get; set; }
        public String Descripcion { get; set; }

    }
}