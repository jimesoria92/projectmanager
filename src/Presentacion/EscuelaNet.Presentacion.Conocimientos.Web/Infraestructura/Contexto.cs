﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();
        public List<Categoria> Categorias { get; set; }
        public List<Conocimiento> Conocimientos { get; set; }

        private Contexto()
        {

            this.Categorias = new List<Categoria>();
            Categorias.Add(new Categoria("Programación", "lorem ipsum dolor sit amet consectetur adipiscing elit"));
            this.Categorias[0].AgregarConocimiento(new Conocimiento("Java"));
            this.Categorias[0].AgregarConocimiento(new Conocimiento("C#"));
            


            Categorias.Add(new Categoria("Seguridad Informática", "lorem ipsum dolor sit amet consectetur adipiscing elit"));
            this.Categorias[1].AgregarConocimiento(new Conocimiento("Seguridad Hardware"));
            this.Categorias[1].AgregarConocimiento(new Conocimiento("Seguridad de Software"));

            Categorias.Add(new Categoria("Base de Datos", "lorem ipsum dolor sit amet consectetur adipiscing elit"));
            this.Categorias[2].AgregarConocimiento(new Conocimiento("SQL Server"));
            this.Categorias[2].AgregarConocimiento(new Conocimiento("PostGresql"));



        }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }

    }
}
