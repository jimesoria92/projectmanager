﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class UnidadesIndexModel
    {
        public string Titulo { get; set; }

        public int IdCliente { get; set; }

        public List<UnidadDeNegocio> Unidades { get; set; }
    }
}