﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class DireccionesIndexModel
    {
        public string Titulo { get; set; }

        public int IdUnidad { get; set; }

        public int IdCliente { get; set; }

        public List<Direccion> Direcciones { get; set; }
    }
}