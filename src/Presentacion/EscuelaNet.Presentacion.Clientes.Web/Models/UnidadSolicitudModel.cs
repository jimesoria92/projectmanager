﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class UnidadSolicitudModel
    {
        public Solicitud Solicitud { get; set; }

        public List<UnidadDeNegocio> Unidades { get; set; }

        public UnidadDeNegocio Unidad { get; set; }

        public List<Solicitud> Solicitudes { get; set; }

    }
}