﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class NuevoEquipoSkillModel
    {
        public int IdSkill { get; set; }
        public int IdEquipo { get; set; }
        public string DescripcionSkill { get; set; }
        public string NombreEquipo { get; set; }
        public List<Skills> Skills { get; set; }
    }
}