﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EsculaNet.Infraestructura.Programadores.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class SkillsController : Controller
    {
        private ISkillRepository Repositorio = new SkillsRepository();
        // GET: Skills
        public ActionResult Index()
        {
            var skill = Repositorio.ListSkills();
           
            var model = new SkillsIndexModel()
            {
                Titulo = "Nuevo Conocimiento.",
                Skills = skill
            };

            return View(model);
        }

        // GET: Skills/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Skills/Create
        public ActionResult Create()
        {
            var model = new NuevoSkillsModel();
            return View(model);
        }

        // POST: Skills/Create
        [HttpPost]
        public ActionResult Create(NuevoSkillsModel model)
        {
            if (!string.IsNullOrEmpty(model.Descripcion))
            {
                try
                {
                    var conocimiento = new Skills(model.Descripcion, model.Grados);
                    Repositorio.Add(conocimiento);

                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Conocimiento Creado Correctamente";

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["Error"] = e.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        // GET: Skills/Edit/5
        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Conocimiento no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var skill = Repositorio.GetSkill(id);
                var model = new NuevoSkillsModel()
                {
                    Descripcion = skill.Descripcion,
                    Grados = skill.Grados,
                    IdSkills = id
                };

                return View(model);
            }   
        }

        // POST: Skills/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoSkillsModel model)
        {
            if (!string.IsNullOrEmpty(model.Descripcion))
            {
                try
                {
                    var skill = Repositorio.GetSkill(model.IdSkills);
                    skill.Descripcion = model.Descripcion;
                    skill.Grados = model.Grados;

                    Repositorio.Update(skill);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Conocimiento Modificado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                    return View(model);

                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
     
        // GET: Skills/Delete/5
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Conocimiento no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var skill = Repositorio.GetSkill(id);
                var model = new NuevoSkillsModel()
                {
                    Descripcion = skill.Descripcion,
                    Grados = skill.Grados,
                    IdSkills = id
                };

                return View(model);
            }
    
        }

        // POST: Skills/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoSkillsModel model)
        {
            try
            {
                var skill = Repositorio.GetSkill(model.IdSkills);
                Repositorio.Delete(skill);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Conocimiento borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
