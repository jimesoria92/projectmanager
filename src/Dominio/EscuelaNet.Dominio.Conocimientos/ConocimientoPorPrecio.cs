﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class ConocimientoPorPrecio : Entity
    {
        public DateTime Fecha { set; get;}
        public Nivel Nivel { set; get;}
        public string Moneda { get; set; }
        public Double ValorNominal { get; set; }

     
        public ConocimientoPorPrecio(Double valorNominal, string moneda, Nivel nivel)
        {
            if (string.IsNullOrEmpty(moneda))
            {
                throw new ArgumentException("message", nameof(moneda));
            }
            if (valorNominal <= 0)
            {
                throw new ExcepcionDeConocimiento("Un precio debe ser mayor que 0");
            }
            this.ValorNominal = ValorNominal;
            this.Moneda = moneda;
            
        }
        public ConocimientoPorPrecio()
        {

        }
        public void InflacionAnual(ConocimientoPorPrecio cp, ConocimientoPorPrecio cpanterior, double porcentaje)
        {
            porcentaje = porcentaje * 100;
            double inflacion = cpanterior.ValorNominal * porcentaje;


            if (cp.ValorNominal < inflacion)
            {
                Console.WriteLine("El precio perdio con respecto a la inflacion");
            }
            if (cp.ValorNominal > inflacion)
            {
                Console.WriteLine("El precio le gano a la inflacion");
            }
        }
        public void TrimestreAnterior(ConocimientoPorPrecio cp, ConocimientoPorPrecio cpanterior)
        {
            int tri1 = 3;
            int tri2 = 6;
            int tri3 = 9;
            int tri4 = 12;



            if (cpanterior.Fecha.Month == tri1)
            {
                if (cpanterior.Fecha.Month < cp.Fecha.Month)
                {
                    if (cp.ValorNominal > cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio subio");
                    }
                    if (cp.ValorNominal < cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio bajo");
                    }
                    if (cp.ValorNominal == cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio se mantuvo");
                    }

                }
            }

            if (cpanterior.Fecha.Month == tri2)
            {
                if (cpanterior.Fecha.Month < cp.Fecha.Month)
                {
                    if (cp.ValorNominal > cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio subio");
                    }
                    if (cp.ValorNominal < cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio bajo");
                    }
                    if (cp.ValorNominal == cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio se mantuvo");
                    }

                }
            }

            if (cpanterior.Fecha.Month == tri3)
            {
                if (cpanterior.Fecha.Month < cp.Fecha.Month)
                {
                    if (cp.ValorNominal > cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio subio");
                    }
                    if (cp.ValorNominal < cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio bajo");
                    }
                    if (cp.ValorNominal == cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio se mantuvo");
                    }

                }
            }

            if (cpanterior.Fecha.Month == tri4)
            {
                if (cpanterior.Fecha.Year < cp.Fecha.Year)
                {
                    if (cp.ValorNominal > cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio subio");
                    }
                    if (cp.ValorNominal < cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio bajo");
                    }
                    if (cp.ValorNominal == cpanterior.ValorNominal)
                    {
                        Console.WriteLine("El precio se mantuvo");
                    }

                }
            }


        }

    }

}
