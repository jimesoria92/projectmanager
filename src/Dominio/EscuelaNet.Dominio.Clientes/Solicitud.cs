﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public class Solicitud : Entity, IAggregateRoot
    {
        public string  Titulo { get; set; }

        public string Descripcion { get; set; }

        public EstadoSolicitud Estado { get; set; }
       
        public IList<UnidadDeNegocio> UnidadesDeNegocio { get; set; }        

        private Solicitud() { }

        public Solicitud(string titulo, string descripcion)
        {
            this.Titulo = titulo ?? throw new System.ArgumentNullException(nameof(titulo));
            this.Descripcion = descripcion ?? throw new System.ArgumentNullException(nameof(descripcion));
            this.Estado = EstadoSolicitud.Borrador;            
        }

        public void CambiarEstado(EstadoSolicitud estadoNuevo)
        {
            if (this.Estado == EstadoSolicitud.Borrador && estadoNuevo == EstadoSolicitud.Desarrollo)
            {
                this.Estado = EstadoSolicitud.Desarrollo;
            }
            else if (this.Estado == EstadoSolicitud.Desarrollo && estadoNuevo == EstadoSolicitud.Produccion)
            {
                this.Estado = EstadoSolicitud.Produccion;
            }
            else if (this.Estado == EstadoSolicitud.Produccion && estadoNuevo == EstadoSolicitud.Deprecado)
            {
                this.Estado = EstadoSolicitud.Deprecado;
            }
            else if (this.Estado == EstadoSolicitud.Desarrollo && estadoNuevo == EstadoSolicitud.Abandonado)
            {
                this.Estado = EstadoSolicitud.Abandonado;
            }
            else if (this.Estado == EstadoSolicitud.Abandonado && estadoNuevo == EstadoSolicitud.Borrador)
            {
                this.Estado = EstadoSolicitud.Borrador;
            }
            else if (this.Estado == estadoNuevo)
            {
                this.Estado = estadoNuevo;
            }
            else
            {
                string mensaje = "No se puede pasar una solicitud con estado "+this.Estado.ToString()+" al estado "+estadoNuevo.ToString();
                throw new ExcepcionDeCliente(mensaje);
            }
        }

        public void pushUnidad(UnidadDeNegocio unidad)
        {
            if (this.UnidadesDeNegocio==null)
            {
                this.UnidadesDeNegocio = new List<UnidadDeNegocio>();
            }
            
            if (!this.UnidadesDeNegocio.Contains(unidad))
            {                
                    this.UnidadesDeNegocio.Add(unidad);                                
            }
            else
            {
                throw new ExcepcionDeCliente("Esta solicitud ya esta vinculada a la unidad");
            }
        }

        public void pullUnidad(UnidadDeNegocio unidad)
        {
            if (this.UnidadesDeNegocio != null)
            {                
                UnidadDeNegocio u = new UnidadDeNegocio("","","","","");
                foreach (var unid in this.UnidadesDeNegocio)
                {
                    if (unid.ID == unidad.ID)
                    {
                        u = unid;
                    }
                }
                this.UnidadesDeNegocio.Remove(u);             
            }
        }

    }
}
