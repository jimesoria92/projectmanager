﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Clientes.Repositorios
{
    public class ClientesRepository : IClienteRepository
    {
        private ClienteRepository _contexto = new ClienteRepository();
        public IUnitOfWork UnitOfWork => _contexto;

        public Cliente Add(Cliente cliente)
        {
            _contexto.Clientes.Add(cliente);
            return cliente;
        }

        public void Delete(Cliente cliente)
        {
            _contexto.Clientes.Remove(cliente);
        }

        public void DeleteUnidadDeNegocio(UnidadDeNegocio unidad)
        {
            _contexto.Unidades.Remove(unidad);
        }

        public void DeleteDireccion(Direccion direccion)
        {
            _contexto.Direcciones.Remove(direccion);
        }

        public Cliente GetCliente(int id)
        {
            var cliente = _contexto.Clientes.Find(id);
            if (cliente != null)
            {
                _contexto.Entry(cliente)
                    .Collection(c => c.Unidades).Load();
            }
            return cliente;
        }


        public UnidadDeNegocio GetUnidadDeNegocio(int id)
        {
            var unidad = _contexto.Unidades.Find(id);
            if (unidad != null)
            {
                _contexto.Entry(unidad)
                    .Reference(un => un.Cliente).Load();

                _contexto.Entry(unidad)
                    .Collection(un => un.Direcciones).Load();
            }
            return unidad;
        }
        public Direccion GetDireccion(int id)
        {
            var direccion = _contexto.Direcciones.Find(id);
            if (direccion != null)
            {
                _contexto.Entry(direccion)
                    .Reference(d => d.Unidad).Load();
            }
            return direccion;
        }

        public List<Cliente> ListCliente()
        {
            return _contexto.Clientes.ToList();
        }

        public void Update(Cliente cliente)
        {
            _contexto.Entry(cliente).State =
                        EntityState.Modified;
        }

    }
}
