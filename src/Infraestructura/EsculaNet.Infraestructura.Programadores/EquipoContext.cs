﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.SeedWoork;
using EsculaNet.Infraestructura.Programadores.EntityTypeConfigurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores
{
    public class EquipoContext : DbContext, IUnitOfWork
    {
        public DbSet<Equipo> Equipos { get; set; }
        public DbSet<Skills> Skills { get; set; }
        public DbSet<Programador> Programadores { get; set; }
        public EquipoContext() : base("EquipoContext")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EquiposEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SkillsEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ProgramadoresEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
