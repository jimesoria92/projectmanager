﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Infraestructura.Capacitacion.Singleton
{
    public sealed class CapacitacionSingleton: IUnitOfWork
    {
        private static CapacitacionSingleton _instancia = new CapacitacionSingleton();

        public List<Lugar> Lugares { get; set; }

        private CapacitacionSingleton()
        {
            this.Lugares = new List<Lugar>();
            this.Lugares.Add(new Lugar(20,"Las Piedras","123","","","San Miguel de Tucuman","Tucuman","Argentina"));
            this.Lugares.Add(new Lugar(20, "San Lorenzo", "3234", "", "", "Yerba Buena", "Tucuman", "Argentina"));
        }

        public static CapacitacionSingleton Instancia
        {
            get { return _instancia; }
        }

        public int SaveChanges()
        {
            return 1;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}